import time

from telethon import TelegramClient, sync
from telethon.tl.functions.photos import UploadProfilePhotoRequest, DeletePhotosRequest
from config import *
from utils import *


client = TelegramClient("<Session name>", api_id, api_hash)
client.start()


prev_update_time = ""

while True:
    if time_has_changed(prev_update_time):
        prev_update_time = convert_time_to_string(datetime.now())
        client(DeletePhotosRequest(client.get_profile_photos('me')))
        file = client.upload_file(f"time_images/{prev_update_time.replace(':', '_')}.jpg")
        client(UploadProfilePhotoRequest(file))
    time.sleep(1)
